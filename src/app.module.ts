import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Connection } from 'typeorm'
import { DataModule } from './modules/data/data.module'
import { ServiceModule } from './modules/service/service.module'
import { WebModule } from './modules/web/web.module'

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    DataModule,
    ServiceModule,
    WebModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {
  constructor(private readonly connection: Connection) {}
}
