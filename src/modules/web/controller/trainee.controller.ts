import { RequestTrainerPostData } from './../../service/model/trainee/request-trainer.post.data'
import { Controller, Get, Post, Body, UseFilters, HttpException, Param } from '@nestjs/common'
import { HttpExceptionFilter } from '../filter/http.exception.filter'
import { TraineeService } from 'src/modules/service/component/trainee.service'
import { TraineeModel } from 'src/modules/service/model/trainee/trainee.model'
import { TraineePostData } from 'src/modules/service/model/trainee/trainee.post.data'

@Controller('trainees')
@UseFilters(new HttpExceptionFilter())
export class TraineeController {

  constructor(private readonly traineeService: TraineeService) {}

  @Get()
  getTrainees(): Promise<TraineeModel[]> {
    try {
      return this.traineeService.getTrainees()
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Get('/:id')
  getTrainee(@Param('id') id: string): Promise<TraineeModel> {
    try {
      return this.traineeService.getTrainee(id)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Post()
  addTrainee(@Body() traineePostData: TraineePostData): Promise<TraineePostData> {
    try {
      return this.traineeService.addTrainee(traineePostData)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Post('/:traineeId/trainers/')
  requestTrainer(@Param('traineeId') traineeId: string, @Body() requestTrainerPostData: RequestTrainerPostData): Promise<TraineePostData> {
    try {
      return this.traineeService.requestTrainer(traineeId, requestTrainerPostData.trainerId)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }
}
