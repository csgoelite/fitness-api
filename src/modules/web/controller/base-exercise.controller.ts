import { BaseExercisePostData } from '../../service/model/base-exercise/base-exercise.post.data'
import { BaseExerciseModel } from '../../service/model/base-exercise/base-exercise.model'
import { HttpExceptionFilter } from './../filter/http.exception.filter'
import { Controller, UseFilters, HttpException, Get, Body, Post } from '@nestjs/common'
import { BaseExerciseService } from './../../service/component/base-exercise.service'

@Controller('baseexercises')
@UseFilters(new HttpExceptionFilter())
export class BaseExerciseController {

  constructor(private readonly baseExerciseService: BaseExerciseService) {}

  @Get()
  getBaseExercises(): Promise<BaseExerciseModel[]> {
    try {
      return this.baseExerciseService.getBaseExercises()
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }
}
