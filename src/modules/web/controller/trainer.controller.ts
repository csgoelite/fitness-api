import { WorkoutSchemaService } from './../../service/component/workout-schema.service'
import { WorkoutSchemaModel } from './../../service/model/workout-schema/workout-schema.model'
import { WorkoutSchemaPostData } from './../../service/model/workout-schema/workout-schema.post.data'
import { Controller, Get, Post, Body, UseFilters, HttpException, Param, Put, Delete } from '@nestjs/common'
import { HttpExceptionFilter } from '../filter/http.exception.filter'
import { TrainerService } from 'src/modules/service/component/trainer.service'
import { TrainerBaseDataModel } from 'src/modules/service/model/trainer/trainer.base.data.model'
import { TrainerPostData } from 'src/modules/service/model/trainer/trainer.post.data'
import { WorkoutPlanPostData } from 'src/modules/service/model/workout-plan/workout-plan.post.data'
import { WorkoutPlanService } from 'src/modules/service/component/workout-plan.service'
import { WorkoutPlanModel } from 'src/modules/service/model/workout-plan/workout-plan.model'
import { BaseExerciseService } from 'src/modules/service/component/base-exercise.service'
import { BaseExercisePostData } from 'src/modules/service/model/base-exercise/base-exercise.post.data'
import { BaseExerciseModel } from 'src/modules/service/model/base-exercise/base-exercise.model'

@Controller('trainers')
@UseFilters(new HttpExceptionFilter())
export class TrainerController {

  constructor(
    private readonly baseExerciseService: BaseExerciseService,
    private readonly trainerService: TrainerService,
    private readonly workoutPlanService: WorkoutPlanService,
    private readonly workoutSchemaService: WorkoutSchemaService,
  ) {}

  @Get()
  getAllTrainers(): Promise<TrainerBaseDataModel[]> {
    try {
      return this.trainerService.getTrainers()
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Get('/:id')
  getTrainer(@Param('id') id: string): Promise<TrainerBaseDataModel> {
    try {
      return this.trainerService.getTrainer(id)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Get('/:id/trainees')
  getTrainerWithTrainees(@Param('id') id: string): Promise<TrainerBaseDataModel> {
    try {
      return this.trainerService.getTrainerWithTrainees(id)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Get('/:id/workoutplans')
  getTrainerWithWorkoutPlans(@Param('id') id: string): Promise<TrainerBaseDataModel> {
    try {
      return this.trainerService.getTrainerWithWorkoutPlans(id)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Get('/:id/workoutschemas')
  getTrainerWithWorkoutSchemas(@Param('id') id: string): Promise<TrainerBaseDataModel> {
    try {
      return this.trainerService.getTrainerWithWorkoutSchemas(id)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Get('/:id/workoutplans/:workoutPlanId')
  getWorkoutPlan(@Param('workoutPlanId') workoutPlanId: string): Promise<WorkoutPlanModel> {
    try {
      return this.workoutPlanService.getWorkoutPlan(workoutPlanId)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Get('/:id/workoutschemas/:workoutSchemaId')
  getWorkoutSchema(@Param('workoutSchemaId') workoutSchemaId: string): Promise<WorkoutSchemaModel> {
    try {
      return this.workoutSchemaService.getWorkoutSchema(workoutSchemaId)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Get('/:id/baseexercises')
  getTrainerWithBaseExercises(@Param('id') id: string): Promise<TrainerBaseDataModel> {
    try {
      return this.trainerService.getTrainerWithBaseExercises(id)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Post('/:id/baseexercises')
  addBaseExercise(@Param('id') id: string, @Body() baseExercisePostData: BaseExercisePostData): Promise<BaseExerciseModel> {
    try {
      return this.baseExerciseService.addBaseExercise(id, baseExercisePostData)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Post()
  addTrainer(@Body() trainerPostData: TrainerPostData): Promise<TrainerPostData> {
    try {
      return this.trainerService.addTrainer(trainerPostData)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Post('/:id/workoutschemas')
  addWorkoutSchema(@Param('id') id: string, @Body() workoutSchemaPostData: WorkoutSchemaPostData): Promise<WorkoutSchemaModel> {
    try {
      return this.workoutSchemaService.addWorkoutSchema(id, workoutSchemaPostData)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Post('/:id/workoutplans')
  addWorkoutPlan(@Param('id') id: string, @Body() workoutPlanPostData: WorkoutPlanPostData): Promise<any> {
    try {
      return this.workoutPlanService.addWorkoutPlan(id, workoutPlanPostData)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Put('/:id/trainees/:traineeId')
  acceptTrainee(@Param('id') id: string, @Param('traineeId') traineeId: string): Promise<TrainerPostData> {
    try {
      return this.trainerService.acceptTrainee(id, traineeId)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Put('/:id')
  updateTrainer(@Param('id') id: string, @Body() trainerPostData: TrainerPostData): Promise<any> {
    try {
      return this.trainerService.updateTrainer(id, trainerPostData)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Put('/:id/workoutschemas/:schemaid')
  updateWorkoutSchema(
    @Param('id') id: string,
    @Param('schemaid') schemaid: string,
    @Body() workoutSchemaPostData: WorkoutSchemaPostData,
  ): Promise<string> {
    try {
      return this.workoutSchemaService.updateWorkoutSchema(id, schemaid, workoutSchemaPostData)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Delete('/:id/workoutschemas/:schemaid')
  deleteWorkoutSchema(@Param('schemaid') schemaid: string): Promise<any> {
    try {
      return this.workoutSchemaService.deleteWorkoutSchema(schemaid)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Delete('/:id/workoutplans/:planid')
  deleteWorkoutPlan(@Param('planid') planid: string): Promise<any> {
    try {
      return this.workoutPlanService.deleteWorkoutPlan(planid)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }

  @Delete('/:id/trainees/:traineeId')
  rejectTrainee(@Param('id') id: string, @Param('traineeId') traineeId: string): Promise<TrainerPostData> {
    try {
      return this.trainerService.rejectTrainee(id, traineeId)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }
}
