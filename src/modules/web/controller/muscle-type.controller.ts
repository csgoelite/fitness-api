import { MuscleTypeService } from './../../service/component/muscle-type.service'
import { MuscleTypeModel } from '../../service/model/muscle-type/muscle-type.model'
import { HttpExceptionFilter } from './../filter/http.exception.filter'
import { Controller, UseFilters, HttpException, Get } from '@nestjs/common'

@Controller('muscletypes')
@UseFilters(new HttpExceptionFilter())
export class MuscleTypeController {

  constructor(private readonly muscleTypeService: MuscleTypeService) {}

  @Get()
  getMuscleTypes(): Promise<MuscleTypeModel[]> {
    try {
      return this.muscleTypeService.getMuscleTypes()
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }
}
