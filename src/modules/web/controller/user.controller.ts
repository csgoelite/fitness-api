import { UserDataService } from '../../service/component/user-data.service'
import { Controller, UseFilters, HttpException, Get, Param } from '@nestjs/common'
import { HttpExceptionFilter } from '../filter/http.exception.filter'

@Controller('users')
@UseFilters(new HttpExceptionFilter())
export class UserController {

  constructor(private readonly userDataService: UserDataService) {}

  @Get('/:extUserId/userdata')
  getuserDataByExtUserId(@Param('extUserId') extUserId: string): Promise<{ userType: string, userId: number }> {
    try {
      return this.userDataService.getUserData(extUserId)
    } catch (err) {
      throw new HttpException(err, 500)
    }
  }
}
