import {MuscleTypeController} from './controller/muscle-type.controller'
import {UserController} from './controller/user.controller'
import {BaseExerciseController} from './controller/base-exercise.controller'
import { Module } from '@nestjs/common'
import { TraineeController } from './controller/trainee.controller'
import { TrainerController } from './controller/trainer.controller'

@Module({
  imports: [
  ],
  providers: [
  ],
  controllers: [
    BaseExerciseController,
    MuscleTypeController,
    TraineeController,
    TrainerController,
    UserController,
  ],
})
export class WebModule {}
