import { WorkoutSchemaService } from './component/workout-schema.service'
import { MuscleTypeService } from './component/muscle-type.service'
import { WorkoutService } from './component/workout.service'
import { UserDataService } from './component/user-data.service'
import { WorkoutPlanService } from './component/workout-plan.service'
import { Module, Global } from '@nestjs/common'
import { BaseExerciseService } from './component/base-exercise.service'
import { TraineeService } from './component/trainee.service'
import { TrainerService } from './component/trainer.service'

@Global()
@Module({
  imports: [
  ],
  providers: [
    BaseExerciseService,
    MuscleTypeService,
    TraineeService,
    TrainerService,
    UserDataService,
    WorkoutPlanService,
    WorkoutService,
    WorkoutSchemaService,
  ],
  exports: [
    BaseExerciseService,
    MuscleTypeService,
    TraineeService,
    TrainerService,
    UserDataService,
    WorkoutPlanService,
    WorkoutService,
    WorkoutSchemaService,
  ],
})
export class ServiceModule {}
