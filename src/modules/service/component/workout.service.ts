import {SetInfoRepository} from '../../data/repository/set-info.repository'
import {AssignedExerciseRepository} from '../../data/repository/assigned-exercise.repository'
import {SetInfo} from '../../data/model/set-info.entity'
import {WorkoutPostData} from '../model/workout/workout.post.data'
import {Workout} from '../../data/model/workout.entity'
import { WorkoutRepository } from '../../data/repository/workout.repository'
import { WorkoutModel } from '../model/workout/workout.model'
import { Injectable, HttpException } from '@nestjs/common'
import { AssignedExercise } from 'src/modules/data/model/assigned-exercise.entity'

@Injectable()
export class WorkoutService {

  constructor(
    private readonly workoutRepository: WorkoutRepository,
    private readonly assignedExerciseRepository: AssignedExerciseRepository,
    private readonly setInfoRepository: SetInfoRepository,
  ) {}

  async getWorkouts(): Promise<any[]> {
    const rawWorkouts: Workout[] = await this.workoutRepository.getWorkouts()

    return rawWorkouts.map( rawWorkout => {
      return rawWorkout
    })
  }

  async addWorkout(workoutPostData: WorkoutPostData): Promise<any> {
    const rawWorkout: Partial<Workout> = {
      finished: new Date(),
      isExtended: workoutPostData.isExtended,
      workoutPlan: { id: undefined },
      trainee: { id: workoutPostData.traineeId },
      assignedExercises: [],
    }

    const newWorkout: Workout = await this.workoutRepository.addWorkout(rawWorkout).catch( err => {
      throw new HttpException(err, 500)
    })

    workoutPostData.assignedExercises.map( async rawAssignedExercise => {
      const assignedExercise: Partial<AssignedExercise> = {
        baseExercise: { id: rawAssignedExercise.baseExerciseId },
        workout: { id: newWorkout.id },
        setInfos: [],
      }

      const newAssignedExercise: AssignedExercise = await this.assignedExerciseRepository.addAssignedExercise(assignedExercise).catch( err => {
        throw new HttpException(err, 500)
      })

      rawAssignedExercise.setInfos.map( async rawSetInfo => {
        const setInfo: Partial<SetInfo> = {
          assignedExercise: { id: newAssignedExercise.id },
          reps: rawSetInfo.reps,
          weight: rawSetInfo.weight,
        }

        const newSetInfo: SetInfo = await this.setInfoRepository.addSetInfo(setInfo).catch( err => {
          throw new HttpException(err, 500)
        })
      })
    })

    return newWorkout
  }
}
