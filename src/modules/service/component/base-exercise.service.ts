import { MuscleTypeBaseExercise } from './../../data/model/muscle-type-base-exercise.entity'
import { MuscleTypeBaseExerciseRepository } from './../../data/repository/muscle-type-base-exercise.repository'
import { Injectable, HttpException } from '@nestjs/common'
import { BaseExerciseModel } from '../model/base-exercise/base-exercise.model'
import { BaseExercisePostData } from '../model/base-exercise/base-exercise.post.data'
import { BaseExerciseRepository } from '../../data/repository/base-exercise.repository'
import { BaseExercise } from '../../data/model/base-exercise.entity'

@Injectable()
export class BaseExerciseService {

  constructor(
    private readonly baseExerciseRepository: BaseExerciseRepository,
    private readonly muscleTypeBaseExerciseRepository: MuscleTypeBaseExerciseRepository,
  ) {}

  async getBaseExercises(): Promise<BaseExerciseModel[]> {
    const rawExercises: BaseExercise[] = await this.baseExerciseRepository.getBaseExercises()
    const convertedBaseExercises: BaseExerciseModel[] = rawExercises.map( rawExercise => {
      return { ...rawExercise } as BaseExerciseModel
    })

    return convertedBaseExercises
  }

  async addBaseExercise(id: string, exercisePostData: BaseExercisePostData): Promise<BaseExerciseModel> {
    const exercise: Partial<BaseExercise> = { ...exercisePostData, trainer: { id } }
    const newExercise = await this.baseExerciseRepository.addBaseExercise(exercise).catch( err => {
      throw new HttpException(err, 500)
    })

    exercisePostData.muscleTypeIds.map( async muscleTypeId => {
      const baseExercise: Partial<MuscleTypeBaseExercise> = { baseExerciseId: newExercise.id, muscleTypeId }
      await this.muscleTypeBaseExerciseRepository.addMuscleTypeBaseExerciseRelation(baseExercise)
    })

    return { ...newExercise } as BaseExerciseModel
  }
}
