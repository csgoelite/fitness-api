import { UpdateResult, DeleteResult } from 'typeorm'
import { TraineeStatusModel } from './../model/trainee/trainee-status.model'
import { ResourceUtil } from './../util/resource.util'
import { RelationStatus } from './../../data/model/relation-status.model'
import { TraineeModel } from 'src/modules/service/model/trainee/trainee.model'
import { TrainerTraineeRepository } from './../../data/repository/trainer-trainee.repository'
import { TrainerTrainee } from './../../data/model/trainer-trainee.entity'
import { TrainerWorkoutSchemasDataModel } from '../model/trainer/trainer.workoutschemas.data.model'
import { TrainerBaseExercisesDataModel } from '../model/trainer/tranier.baseexercises.data.model'
import { TrainerWorkoutPlansDataModel } from '../model/trainer/tranier.workoutplans.data.model'
import { TrainerTraineesDataModel } from '../model/trainer/tranier.trainees.data.model'
import { TrainerPostData } from '../model/trainer/trainer.post.data'
import { Injectable, HttpException } from '@nestjs/common'
import { Trainer } from '../../data/model/trainer.entity'
import { TrainerRepository } from '../../data/repository/trainer.repository'
import { TrainerBaseDataModel } from '../model/trainer/trainer.base.data.model'

@Injectable()
export class TrainerService {

  constructor(
    private readonly trainerRepository: TrainerRepository,
    private readonly trainerTraineeRepository: TrainerTraineeRepository,
  ) {}

  async getTrainers(): Promise<TrainerBaseDataModel[]> {
    const rawTrainers: Trainer[] = await this.trainerRepository.getTrainers()
    const trainers: TrainerBaseDataModel[] = rawTrainers.map( rawTrainer => {
      return { ...rawTrainer } as TrainerBaseDataModel
    })

    return trainers
  }

  async getTrainer(id: string): Promise<TrainerBaseDataModel> {
    const trainer: Trainer = await this.trainerRepository.getTrainer(id)

    ResourceUtil.handleResourceNotFound(trainer, id)

    return trainer as TrainerBaseDataModel
  }

  async getTrainerWithTrainees(id: string): Promise<TrainerTraineesDataModel> {
    const trainerWithTrainees: Trainer = await this.trainerRepository.getTrainerWithTrainees(id)

    ResourceUtil.handleResourceNotFound(trainerWithTrainees, id)

    const trainees: TraineeStatusModel[] = []
    const pendingTrainees: TraineeStatusModel[] = []

    const result = trainerWithTrainees.trainees.map( async trainee => {
      const status: TrainerTrainee = await this.trainerTraineeRepository.getTrainerStatusByTrainerIdAndTraineeId(
        trainerWithTrainees.id,
        trainee.id,
      )
      const extendedTrainee = { ...trainee, relationStatusId: status.id }

      status.relationStatus === RelationStatus.ACCEPTED ? trainees.push(extendedTrainee) : pendingTrainees.push(extendedTrainee)

      return
    })
    await Promise.all(result)

    return {
      id: trainerWithTrainees.id,
      name: trainerWithTrainees.name,
      age: trainerWithTrainees.age,
      extUserId: trainerWithTrainees.extUserId,
      trainees,
      pendingTrainees,
    }
  }

  async getTrainerWithWorkoutPlans(id: string): Promise<TrainerWorkoutPlansDataModel> {
    const trainer: Trainer = await this.trainerRepository.getTrainerWithWorkoutPlans(id)

    ResourceUtil.handleResourceNotFound(trainer, id)

    return trainer as TrainerBaseDataModel as TrainerWorkoutPlansDataModel
  }

  async getTrainerWithWorkoutSchemas(id: string): Promise<TrainerWorkoutSchemasDataModel> {
    const trainer: Trainer = await this.trainerRepository.getTrainerWithWorkoutSchemas(id)

    ResourceUtil.handleResourceNotFound(trainer, id)

    return trainer as TrainerBaseDataModel as TrainerWorkoutSchemasDataModel
  }

  async getTrainerWithBaseExercises(id: string): Promise<TrainerBaseExercisesDataModel> {
    const trainer: Trainer = await this.trainerRepository.getTrainerWithBaseExercises(id)

    ResourceUtil.handleResourceNotFound(trainer, id)

    return trainer as TrainerBaseDataModel as TrainerBaseExercisesDataModel
  }

  async addTrainer(trainerPostData: TrainerPostData): Promise<TrainerBaseDataModel> {
    return await this.trainerRepository.addTrainer({ ...trainerPostData }).catch( err => {
      throw new HttpException(err, 500)
    }) as TrainerBaseDataModel
  }

  async updateTrainer(id: string, trainerPostData: TrainerPostData): Promise<UpdateResult> {
    const result: UpdateResult = await this.trainerRepository.updateTrainer(id, trainerPostData)
    ResourceUtil.handleResourceNotFound(result, id)

    return
  }

  async acceptTrainee(trainerId: string, traineeId: string): Promise<TrainerBaseDataModel> {
    const result: UpdateResult = await this.trainerTraineeRepository.acceptTrainee(trainerId, traineeId)
    ResourceUtil.handleResourceNotFound(result, traineeId)

    return
  }

  async rejectTrainee(trainerId: string, traineeId: string): Promise<TrainerBaseDataModel> {
    const result: DeleteResult = await this.trainerTraineeRepository.rejectTrainee(trainerId, traineeId)
    ResourceUtil.handleResourceNotFound(result, traineeId)

    return
  }
}
