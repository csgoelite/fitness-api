import { ResourceUtil } from './../util/resource.util'
import { Trainee } from '../../data/model/trainee.entity'
import { Injectable, HttpException } from '@nestjs/common'
import { getRepository } from 'typeorm'
import { Trainer } from '../../data/model/trainer.entity'

@Injectable()
export class UserDataService {

  async getUserData(extUserIdParam: string): Promise<{ userType: string, userId: number }> {
    let userData

    const trainerData: Trainer = await getRepository(Trainer).findOne({ where: { extUserId: extUserIdParam } })

    if (trainerData) {
      userData = { userType: 'TRAINER', userId: trainerData.id }
    } else {
      const traineeData: Trainee = await getRepository(Trainee).findOne({ where: { extUserId: extUserIdParam } })

      ResourceUtil.handleResourceNotFound(traineeData, extUserIdParam)

      userData = { userType: 'TRAINEE', userId: trainerData.id }
    }

    return userData
  }
}
