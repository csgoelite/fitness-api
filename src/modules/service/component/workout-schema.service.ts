import { ResourceUtil } from './../util/resource.util'
import { UpdateResult, DeleteResult } from 'typeorm'
import { SetInfoSchemaPostData } from './../model/set-info-schema/set-info-schema.post.data'
import { AssignedExerciseSchemaPostData } from './../model/assigned-exercise-schema/assigned-exercise-schema.post.data'
import { SetInfoSchemaRepository } from './../../data/repository/set-info-schema.repository'
import { AssignedExerciseSchemaRepository } from './../../data/repository/assigned-exercise-schema.repository'
import { WorkoutSchema } from './../../data/model/workout-schema.entity'
import { WorkoutSchemaModel } from '../model/workout-schema/workout-schema.model'
import { Injectable, Logger, HttpException } from '@nestjs/common'
import { WorkoutSchemaRepository } from './../../data/repository/workout-schema.repository'
import { WorkoutSchemaPostData } from '../model/workout-schema/workout-schema.post.data'
import { AssignedExerciseSchema } from 'src/modules/data/model/assigned-exercise-schema.entity'
import { SetInfoSchema } from 'src/modules/data/model/set-info-schema.entity'

@Injectable()
export class WorkoutSchemaService {

  constructor(
    private readonly workoutSchemaRepository: WorkoutSchemaRepository,
    private readonly assignedExerciseSchemaRepository: AssignedExerciseSchemaRepository,
    private readonly setInfoSchemaRepository: SetInfoSchemaRepository,
  ) {}

  async getWorkoutSchema(id: string): Promise<WorkoutSchemaModel> {
    const workoutSchema: WorkoutSchema = await this.workoutSchemaRepository.getWorkoutSchema(id)

    ResourceUtil.handleResourceNotFound(workoutSchema, id)

    return this.getConvertedWorkoutSchemaModel(workoutSchema)
  }

  async getWorkoutSchemas(): Promise<WorkoutSchemaModel[]> {
    const workoutSchemas: WorkoutSchema[] = await this.workoutSchemaRepository.getWorkoutSchemas()

    const workoutSchemaModels: WorkoutSchemaModel[] = workoutSchemas.map( workoutSchema => {
      return this.getConvertedWorkoutSchemaModel(workoutSchema)
    })

    return workoutSchemaModels
  }

  async addWorkoutSchema(id: string, workoutSchemaPostData: WorkoutSchemaPostData): Promise<WorkoutSchemaModel> {
    const newWorkoutSchema: WorkoutSchema = await this.workoutSchemaRepository.addWorkoutSchema(
      {
        name: workoutSchemaPostData.name,
        created: new Date(),
        trainer: { id },
        assignedExerciseSchemas: [],
      },
    ).catch( err => {
      throw new HttpException(err, 500)
    })

    workoutSchemaPostData.assignedExerciseSchemas.map( async assignedExercisesSchema => {
      this.getAssignedExerciseSchema(assignedExercisesSchema, newWorkoutSchema.id)
    })

    return this.getConvertedWorkoutSchemaModel(newWorkoutSchema)
  }

  async updateWorkoutSchema(userId: string, schemaid: string, workoutSchemaUpdateData: WorkoutSchemaPostData): Promise<string> {
    const updatedWorkoutSchema: UpdateResult = await this.workoutSchemaRepository.updateWorkoutSchema(
      schemaid,
      { name: workoutSchemaUpdateData.name },
    )

    ResourceUtil.handleResourceNotFound(updatedWorkoutSchema, schemaid)

    this.getDeletedAssignedExerciseSchemas(schemaid, workoutSchemaUpdateData)

    workoutSchemaUpdateData.assignedExerciseSchemas.map( async assignedExercisesSchema => {
      this.getDeletedSetInfoSchemas(assignedExercisesSchema.id, assignedExercisesSchema.setInfoSchemas)

      if (!assignedExercisesSchema.id) {
        this.getAssignedExerciseSchema(assignedExercisesSchema, schemaid)
      } else {
        assignedExercisesSchema.setInfoSchemas.map( async setInfoSchemaPostData => {
          const setinfoSchema: Partial<SetInfoSchema> = {
            reps: setInfoSchemaPostData.reps,
            weight: setInfoSchemaPostData.weight,
            assignedExerciseSchema: { id: assignedExercisesSchema.id },
          }

          if (!setInfoSchemaPostData.id) {
            const newSetInfoSchema: SetInfoSchema = await this.setInfoSchemaRepository.addSetInfoSchema(setinfoSchema)
          } else {
            const updatedSetInfoSchema: UpdateResult = await this.setInfoSchemaRepository.updateSetInfoSchema(
              setInfoSchemaPostData.id,
              setinfoSchema,
            )
          }
        })
      }
    })

    return
  }

  async deleteWorkoutSchema(id: string): Promise<DeleteResult> {
    const deleteResult: DeleteResult = await this.workoutSchemaRepository.deleteWorkoutSchema(id)
    ResourceUtil.handleResourceNotFound(deleteResult, id)

    return
  }

  private async getDeletedSetInfoSchemas(schemaid: string, setInfoSchemasUpdateData: SetInfoSchemaPostData[]) {
    const setInfoSchemasByAssignedExerciseIds: SetInfoSchema[] = await this.setInfoSchemaRepository.getSetInfoSchemaIdsByAssignedExerciseId(
      schemaid,
    )
    const modifiedSetInfoIds: string[] = setInfoSchemasUpdateData.map( setInfoSchemaUpdateData => {
      return setInfoSchemaUpdateData.id
    })

    const result = await setInfoSchemasByAssignedExerciseIds.filter( setInfoSchemaByAssignedExerciseIds => {
      return !modifiedSetInfoIds.includes(setInfoSchemaByAssignedExerciseIds.id)
    }).map( async setInfoSchemaToDelete => {
      return await this.setInfoSchemaRepository.deleteSetInfoSchema(setInfoSchemaToDelete.id)
    })

    return result
  }

  private async getDeletedAssignedExerciseSchemas(schemaid: string, workoutSchemaUpdateData: WorkoutSchemaPostData) {
    const assignedExerciseSchemasbyWorkoutScchemaId: AssignedExerciseSchema[] =
      await this.assignedExerciseSchemaRepository.getAssignedExerciseIdsByWorkoutSchemaId(schemaid)

    const modifiedAssignedExerciseIds: string[] = workoutSchemaUpdateData.assignedExerciseSchemas.map( assignedExerciseSchema => {
      return assignedExerciseSchema.id
    })

    const result = await assignedExerciseSchemasbyWorkoutScchemaId.filter( assignedExerciseSchemabyWorkoutSchemaId => {
      return !modifiedAssignedExerciseIds.includes(assignedExerciseSchemabyWorkoutSchemaId.id)
    }).map( async assignedExerciseSchemaToDelete => {
      return await this.assignedExerciseSchemaRepository.deleteAssignedExerciseSchema(assignedExerciseSchemaToDelete.id)
    })

    return result
  }

  private async getAssignedExerciseSchema(
    assignedExerciseSchemaPostData: AssignedExerciseSchemaPostData,
    workoutSchemaId: string,
  ): Promise<AssignedExerciseSchema> {
    const newAssignedExerciseSchema: AssignedExerciseSchema = await this.assignedExerciseSchemaRepository.addAssignedExerciseSchema(
      {
        baseExercise: { id: assignedExerciseSchemaPostData.baseExerciseId },
        workoutSchema: { id: workoutSchemaId },
      },
    ).catch( err => {
      throw new HttpException(err, 500)
    })

    assignedExerciseSchemaPostData.setInfoSchemas.map(async (setInfoSchemaPostData) => {
      const newSetInfoSchema: SetInfoSchema = await this.setInfoSchemaRepository.addSetInfoSchema(
        {
          reps: setInfoSchemaPostData.reps,
          weight: setInfoSchemaPostData.weight,
          assignedExerciseSchema: { id: newAssignedExerciseSchema.id },
        },
      ).catch( err => {
        throw new HttpException(err, 500)
      })
    })

    return newAssignedExerciseSchema
  }

  private getConvertedWorkoutSchemaModel(workoutSchema: WorkoutSchema): WorkoutSchemaModel {
    return {
      id: workoutSchema.id,
      name: workoutSchema.name,
      created: workoutSchema.created,
      assignedExerciseSchemas: workoutSchema.assignedExerciseSchemas.map(assignedExerciseSchema => {
        return {
          id: assignedExerciseSchema.id,
          baseExerciseId: assignedExerciseSchema.baseExercise.id,
          name: assignedExerciseSchema.baseExercise.name,
          muscleTypes: assignedExerciseSchema.baseExercise.muscleTypes.map( muscleType => {
            return { id: muscleType.id, name: muscleType.name }
          }),
          setInfoSchemas: assignedExerciseSchema.setInfoSchemas.map(setInfoSchema => {
            return {
              id: setInfoSchema.id,
              reps: setInfoSchema.reps,
              weight: setInfoSchema.weight,
            }
          }),
        }
      }),
    }
  }
}
