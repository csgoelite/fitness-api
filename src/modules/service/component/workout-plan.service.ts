import { WorkoutModel } from './../model/workout/workout.model'
import { TraineeModel } from 'src/modules/service/model/trainee/trainee.model'
import { Trainee } from './../../data/model/trainee.entity'
import { ResourceUtil } from './../util/resource.util'
import { SetInfoRepository } from './../../data/repository/set-info.repository'
import { SetInfoPostData } from '../model/set-info/set-info.post.data'
import { AssignedExerciseRepository } from './../../data/repository/assigned-exercise.repository'
import { WorkoutRepository } from './../../data/repository/workout.repository'
import { SetInfo } from './../../data/model/set-info.entity'
import { AssignedExercise } from './../../data/model/assigned-exercise.entity'
import { Workout } from './../../data/model/workout.entity'
import { Trainer } from './../../data/model/trainer.entity'
import { WorkoutPlanModel } from '../model/workout-plan/workout-plan.model'
import { WorkoutPlan } from './../../data/model/workout-plan.entity'
import { WorkoutPlanPostData } from '../model/workout-plan/workout-plan.post.data'
import { WorkoutPlanRepository } from './../../data/repository/workout-plan.repository'
import { Injectable, HttpException } from '@nestjs/common'
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';

@Injectable()
export class WorkoutPlanService {

  constructor(
    private readonly workoutPlanRepository: WorkoutPlanRepository,
    private readonly workoutRepository: WorkoutRepository,
    private readonly assignedExerciseRepository: AssignedExerciseRepository,
    private readonly setInfoRepository: SetInfoRepository,
  ) {}

  async getWorkoutPlans(): Promise<WorkoutPlanModel[]>  {
    const workoutPlans: WorkoutPlan[] = await this.workoutPlanRepository.getWorkoutPlans()

    return workoutPlans.map( workoutPlan => {
      return this.getConvertedWorkoutPlan(workoutPlan)
    })
  }

  async getWorkoutPlan(id: string): Promise<WorkoutPlanModel>  {
    const workoutPlan: WorkoutPlan = await this.workoutPlanRepository.getWorkoutPlan(id)
    ResourceUtil.handleResourceNotFound(workoutPlan, id)

    return this.getConvertedWorkoutPlan(workoutPlan)
  }

  async addWorkoutPlan(trainerId: string, workoutPlanPostData: WorkoutPlanPostData): Promise<WorkoutPlanModel> {
    const trainer: Partial<Trainer> = { id: trainerId }
    const newWorkoutPlan: WorkoutPlan = await this.workoutPlanRepository.addWorkoutPlan({
      name: workoutPlanPostData.name,
      trainer,
      created: new Date(),
    }).catch( err => {
      throw new HttpException(err, 500)
    })

    const newWorkout: Workout = await this.getWorkout(newWorkoutPlan.id, workoutPlanPostData.workout.traineeId)

    workoutPlanPostData.workout.assignedExercises.map( async rawAssignedExercise => {
      const newAssignedExercise: AssignedExercise = await this.getAssignedExercise(rawAssignedExercise.baseExerciseId, newWorkout.id).catch( err => {
        throw new HttpException(err, 500)
      })

      rawAssignedExercise.setInfos.map( async rawSetInfo => {
        const newSetInfo: SetInfo = await this.getSetInfo(rawSetInfo, newAssignedExercise.id).catch( err => {
          throw new HttpException(err, 500)
        })
      })
    })

    return
  }

  async deleteWorkoutPlan(workoutPlandId: string): Promise<DeleteResult> {
    const result: DeleteResult = await this.workoutPlanRepository.deleteWorkoutPlan(workoutPlandId)
    ResourceUtil.handleResourceNotFound(result, workoutPlandId)

    return
  }

  private getConvertedWorkoutPlan(workoutPlan: WorkoutPlan): WorkoutPlanModel {
    const convertedWorkouts: WorkoutModel[] = workoutPlan.workouts.map( workout => {
      return {
        id: workout.id,
        finished: workout.finished,
        isExtended: workout.isExtended,
        assignedExercises: workout.assignedExercises.map( assignedExercise => {
          return {
            id: assignedExercise.id,
            setInfos: assignedExercise.setInfos.map( setInfo => {
              return {
                id: setInfo.id,
                reps: setInfo.reps,
                weight: setInfo.weight,
              }
            }),
            baseExercise: {
              id: assignedExercise.baseExercise.id,
              name: assignedExercise.baseExercise.name,
              description: assignedExercise.baseExercise.description,
            },
            muscleTypes: assignedExercise.baseExercise.muscleTypes,
          }
        }),
      }
    })

    const trainee: Partial<Trainee> = workoutPlan.workouts.map( workout => {
      return workout.trainee
    })[0]

    const convertedTrainee: TraineeModel = {
      id: trainee.id,
      age: trainee.age,
      extUserId: trainee.extUserId,
      height: trainee.height,
      name: trainee.name,
      weight: trainee.weight,
    }

    const convertedWorkoutPlan: WorkoutPlanModel = {
      id: workoutPlan.id,
      name: workoutPlan.name,
      created: workoutPlan.created,
      trainee: convertedTrainee,
      workouts:  convertedWorkouts,
    }

    return convertedWorkoutPlan
  }

  private async getWorkout(workoutPlanId: string, traineeId: string): Promise<Workout> {
    const workout: Partial<Workout> = {
      finished: new Date(),
      isExtended: false,
      workoutPlan: { id: workoutPlanId },
      trainee: { id: traineeId },
    }

    return await this.workoutRepository.addWorkout(workout)
  }

  private async getAssignedExercise(baseExerciseId: string, workoutId: string): Promise<AssignedExercise> {
    const assignedExercise: Partial<AssignedExercise> = {
      baseExercise: { id: baseExerciseId },
      workout: { id: workoutId },
    }

    return await this.assignedExerciseRepository.addAssignedExercise(assignedExercise)
  }

  private async getSetInfo(setInfoPostData: SetInfoPostData, assignedExerciseId: string): Promise<SetInfo> {
    const setInfo: Partial<SetInfo> = {
      assignedExercise: { id: assignedExerciseId },
      reps: setInfoPostData.reps,
      weight: setInfoPostData.weight,
    }

    return await this.setInfoRepository.addSetInfo(setInfo)
  }
}
