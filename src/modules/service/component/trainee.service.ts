import { ResourceUtil } from './../util/resource.util'
import { TrainerTraineeRepository } from './../../data/repository/trainer-trainee.repository'
import { TraineeRepository } from '../../data/repository/trainee.repository'
import { Injectable, HttpException } from '@nestjs/common'
import { TraineeModel } from '../model/trainee/trainee.model'
import { Trainee } from '../../data/model/trainee.entity'
import { TraineePostData } from '../model/trainee/trainee.post.data'

@Injectable()
export class TraineeService {

  constructor(
    private readonly traineeRepository: TraineeRepository,
    private readonly trainerTraineeRepository: TrainerTraineeRepository,
  ) {}

  async getTrainees(): Promise<TraineeModel[]> {
    const rawTrainees: Trainee[] = await this.traineeRepository.getTrainees()
    const trainees: TraineeModel[] = rawTrainees.map( rawTrainee => {
      return { ...rawTrainee } as TraineeModel
    })

    return trainees
  }

  async getTrainee(id: string): Promise<TraineeModel> {
    const rawTrainee: Trainee = await this.traineeRepository.getTrainee(id)
    ResourceUtil.handleResourceNotFound(rawTrainee, id)

    return { ...rawTrainee } as TraineeModel
  }

  async addTrainee(trainerPostData: TraineePostData): Promise<TraineeModel> {
    const trainee: Partial<Trainee> = { ...trainerPostData }
    const newTrainee = await this.traineeRepository.addTrainee(trainee).catch( err => {
      throw new HttpException(err, 500)
    })

    return { ...newTrainee } as TraineeModel
  }

  async requestTrainer(traineeId: string, trainerId: string): Promise<TraineeModel> {
    const result = await this.trainerTraineeRepository.requestTrainer(traineeId, trainerId).catch( err => {
      throw new HttpException(err, 500)
    })
    ResourceUtil.handleResourceNotFound(result, trainerId)

    return
  }
}
