import { getRepository } from 'typeorm'
import { MuscleType } from './../../data/model/muscle-type.entity'
import { MuscleTypeModel } from '../model/muscle-type/muscle-type.model'
import { Injectable } from '@nestjs/common'

@Injectable()
export class MuscleTypeService {

  async getMuscleTypes(): Promise<MuscleTypeModel[]> {
    return await getRepository(MuscleType).find() as MuscleTypeModel[]
  }
}
