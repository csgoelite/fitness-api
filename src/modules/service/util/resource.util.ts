import { HttpException } from '@nestjs/common'

export class ResourceUtil {

  static handleResourceNotFound(object: any, id: string): void {
    if (!object) {
      throw new HttpException(`Cannot find resource with id ${id}`, 404)
    }
  }
}
