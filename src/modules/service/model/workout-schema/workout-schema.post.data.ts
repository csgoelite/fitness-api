import { ApiModelProperty } from '@nestjs/swagger'
import { AssignedExerciseSchemaPostData } from '../assigned-exercise-schema/assigned-exercise-schema.post.data'

export class WorkoutSchemaPostData {

    @ApiModelProperty()
    name: string

    @ApiModelProperty()
    assignedExerciseSchemas: AssignedExerciseSchemaPostData[]
}
