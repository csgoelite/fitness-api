import { AssignedExerciseSchemaModel } from '../assigned-exercise-schema/assigned-exercise-schema.model'
import { ApiModelProperty } from '@nestjs/swagger'

export class WorkoutSchemaModel {

  @ApiModelProperty()
  id: string

  @ApiModelProperty()
  created: Date

  @ApiModelProperty()
  name: string

  @ApiModelProperty({ isArray: true, type: [AssignedExerciseSchemaModel] })
  assignedExerciseSchemas: AssignedExerciseSchemaModel[]

}
