import { ApiModelProperty } from '@nestjs/swagger'

export class SetInfoSchemaModel {

  @ApiModelProperty()
  reps: number

  @ApiModelProperty()
  weight: number

}
