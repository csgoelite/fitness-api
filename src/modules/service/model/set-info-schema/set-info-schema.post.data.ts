import { ApiModelProperty } from '@nestjs/swagger'

export class SetInfoSchemaPostData {

  @ApiModelProperty()
  id: string

  @ApiModelProperty()
  reps: number

  @ApiModelProperty()
  weight: number

}
