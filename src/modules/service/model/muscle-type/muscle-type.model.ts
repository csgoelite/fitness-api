import {ApiModelProperty} from '@nestjs/swagger'
export class MuscleTypeModel {

  @ApiModelProperty()
  id: string

  @ApiModelProperty()
  name: string
}
