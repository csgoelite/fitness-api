import { SetInfoSchemaPostData } from './../set-info-schema/set-info-schema.post.data'
import { ApiModelProperty } from '@nestjs/swagger'
import { SetInfoPostData } from '../set-info/set-info.post.data'

export class AssignedExerciseSchemaPostData {

    @ApiModelProperty()
    id: string

    @ApiModelProperty()
    baseExerciseId: string

    @ApiModelProperty({ isArray: true, type: [SetInfoPostData] })
    setInfoSchemas: SetInfoSchemaPostData[]
}
