import { MuscleTypeModel } from './../muscle-type/muscle-type.model'
import { SetInfoSchemaModel } from '../set-info-schema/set-info-schema.model'
import { ApiModelProperty } from '@nestjs/swagger'

export class AssignedExerciseSchemaModel {

  @ApiModelProperty()
  id: string

  @ApiModelProperty()
  baseExerciseId: string

  @ApiModelProperty()
  name: string

  @ApiModelProperty({ isArray: true, type: [MuscleTypeModel] })
  muscleTypes: MuscleTypeModel[]

  @ApiModelProperty({ isArray: true, type: [SetInfoSchemaModel] })
  setInfoSchemas: SetInfoSchemaModel[]

}
