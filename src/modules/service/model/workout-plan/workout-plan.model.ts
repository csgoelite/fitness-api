import { WorkoutModel } from './../workout/workout.model'
import { TraineeModel } from 'src/modules/service/model/trainee/trainee.model'
import { ApiModelProperty } from '@nestjs/swagger'

export class WorkoutPlanModel {

  @ApiModelProperty()
  id: string

  @ApiModelProperty()
  name: string

  @ApiModelProperty()
  created: Date

  @ApiModelProperty()
  trainee: TraineeModel

  @ApiModelProperty({ isArray: true, type: [WorkoutModel] })
  workouts: WorkoutModel[]
}
