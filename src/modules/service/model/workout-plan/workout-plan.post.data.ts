import {ApiModelProperty} from '@nestjs/swagger'
import {WorkoutPostData} from '../workout/workout.post.data'
export class WorkoutPlanPostData {

  @ApiModelProperty()
  name: string

  @ApiModelProperty()
  workout: WorkoutPostData
}
