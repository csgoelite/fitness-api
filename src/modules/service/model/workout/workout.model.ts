import { AssignedExerciseModel } from './../assigned-exercise/assigned-exercise.model'
import { ApiModelProperty } from '@nestjs/swagger'

export class WorkoutModel {

  @ApiModelProperty()
  id: string

  @ApiModelProperty()
  finished: Date

  @ApiModelProperty()
  isExtended: boolean

  @ApiModelProperty({ isArray: true, type: [AssignedExerciseModel] })
  assignedExercises: AssignedExerciseModel[]
}
