import {ApiModelProperty} from '@nestjs/swagger'
import {AssignedExercisePostData} from '../assigned-exercise/assigned-exercise.post.data'
export class WorkoutPostData {

  @ApiModelProperty()
  isExtended: boolean

  @ApiModelProperty()
  traineeId: string

  @ApiModelProperty({ isArray: true, type: [AssignedExercisePostData] })
  assignedExercises: AssignedExercisePostData[]
}
