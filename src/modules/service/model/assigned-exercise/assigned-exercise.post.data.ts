import {ApiModelProperty} from '@nestjs/swagger'
import { SetInfoPostData} from '../set-info/set-info.post.data'

export class AssignedExercisePostData {

  @ApiModelProperty()
  baseExerciseId: string

  @ApiModelProperty({ isArray: true, type: [SetInfoPostData] })
  setInfos: SetInfoPostData[]
}
