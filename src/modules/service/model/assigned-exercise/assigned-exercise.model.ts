import { MuscleTypeModel } from './../muscle-type/muscle-type.model'
import { BaseExerciseModel } from './../base-exercise/base-exercise.model'
import { SetInfoModel } from './../set-info/set-info.model'
import { ApiModelProperty } from '@nestjs/swagger'

export class AssignedExerciseModel {

  @ApiModelProperty()
  id: string

  @ApiModelProperty({ isArray: true, type: [SetInfoModel] })
  setInfos: SetInfoModel[]

  @ApiModelProperty()
  baseExercise: BaseExerciseModel

  @ApiModelProperty({ isArray: true, type: [MuscleTypeModel] })
  muscleTypes: MuscleTypeModel[]
}
