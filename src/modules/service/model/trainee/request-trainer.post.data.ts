import { ApiModelProperty } from '@nestjs/swagger'

export class RequestTrainerPostData {

  @ApiModelProperty()
  trainerId: string
}
