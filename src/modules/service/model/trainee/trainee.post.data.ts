import {ApiModelProperty} from '@nestjs/swagger'
export class TraineePostData {

  @ApiModelProperty()
  name: string

  @ApiModelProperty()
  age: number

  @ApiModelProperty()
  extUserId: string

  @ApiModelProperty()
  weight: number

  @ApiModelProperty()
  height: number
}
