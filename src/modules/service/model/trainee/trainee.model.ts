import {ApiModelProperty} from '@nestjs/swagger'

export class TraineeModel {

  @ApiModelProperty()
  id: string

  @ApiModelProperty()
  name: string

  @ApiModelProperty()
  age: number

  @ApiModelProperty()
  extUserId: string

  @ApiModelProperty()
  weight: number

  @ApiModelProperty()
  height: number
}
