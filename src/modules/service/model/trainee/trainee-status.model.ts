import { ApiModelProperty } from '@nestjs/swagger'
import { TraineeModel } from 'src/modules/service/model/trainee/trainee.model'

export class TraineeStatusModel extends TraineeModel {

  @ApiModelProperty()
  relationStatusId: string
}
