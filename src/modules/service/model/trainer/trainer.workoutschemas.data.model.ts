import {WorkoutSchemaModel} from '../workout-schema/workout-schema.model'
import {ApiModelProperty} from '@nestjs/swagger'
import {WorkoutPlanModel} from '../workout-plan/workout-plan.model'
import { TrainerBaseDataModel } from './trainer.base.data.model'

export class TrainerWorkoutSchemasDataModel extends TrainerBaseDataModel {

  @ApiModelProperty({ isArray: true, type: [WorkoutPlanModel] })
  trainees: WorkoutSchemaModel[]
}
