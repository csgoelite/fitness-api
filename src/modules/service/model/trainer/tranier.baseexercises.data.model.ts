import {BaseExerciseModel} from '../base-exercise/base-exercise.model'
import {ApiModelProperty} from '@nestjs/swagger'
import {WorkoutPlanModel} from '../workout-plan/workout-plan.model'
import { TrainerBaseDataModel } from './trainer.base.data.model'

export class TrainerBaseExercisesDataModel extends TrainerBaseDataModel {

  @ApiModelProperty({ isArray: true, type: [BaseExerciseModel] })
  baseExercises: BaseExerciseModel[]
}
