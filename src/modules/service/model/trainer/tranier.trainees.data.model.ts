import { TraineeStatusModel } from './../trainee/trainee-status.model'
import {ApiModelProperty} from '@nestjs/swagger'
import {TraineeModel} from 'src/modules/service/model/trainee/trainee.model'
import { TrainerBaseDataModel } from './trainer.base.data.model'

export class TrainerTraineesDataModel extends TrainerBaseDataModel {

  @ApiModelProperty({ isArray: true, type: [TraineeModel] })
  trainees: TraineeStatusModel[]

  @ApiModelProperty({ isArray: true, type: [TraineeStatusModel] })
  pendingTrainees: TraineeStatusModel[]
}
