import {ApiModelProperty} from '@nestjs/swagger'
export class TrainerPostData {

  @ApiModelProperty()
  name: string

  @ApiModelProperty()
  age: number

  @ApiModelProperty()
  extUserId: string
}
