import {ApiModelProperty} from '@nestjs/swagger'
import {WorkoutPlanModel} from '../workout-plan/workout-plan.model'
import { TrainerBaseDataModel } from './trainer.base.data.model'

export class TrainerWorkoutPlansDataModel extends TrainerBaseDataModel {

  @ApiModelProperty({ isArray: true, type: [WorkoutPlanModel] })
  trainees: WorkoutPlanModel[]
}
