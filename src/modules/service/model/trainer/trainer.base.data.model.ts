import {ApiModelProperty} from '@nestjs/swagger'

export class TrainerBaseDataModel {

  @ApiModelProperty()
  id: string

  @ApiModelProperty()
  name: string

  @ApiModelProperty()
  age: number

  @ApiModelProperty()
  extUserId: string
}
