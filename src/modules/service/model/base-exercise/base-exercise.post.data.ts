import { ApiModelProperty } from '@nestjs/swagger'

export class BaseExercisePostData {

  @ApiModelProperty()
  name: string

  @ApiModelProperty()
  description: string

  @ApiModelProperty({ type: [String] })
  muscleTypeIds: string[]
}
