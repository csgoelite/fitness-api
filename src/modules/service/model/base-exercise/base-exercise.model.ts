import {ApiModelProperty} from '@nestjs/swagger'
export class BaseExerciseModel {

  @ApiModelProperty()
  id: string

  @ApiModelProperty()
  name: string

  @ApiModelProperty()
  description: string
}
