import {ApiModelProperty} from '@nestjs/swagger'
export class SetInfoPostData {

  @ApiModelProperty()
  reps: number

  @ApiModelProperty()
  weight: number
}
