import { ApiModelProperty } from '@nestjs/swagger'

export class SetInfoModel {

  @ApiModelProperty()
  id: string

  @ApiModelProperty()
  reps: number

  @ApiModelProperty()
  weight: number
}
