import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity({ name: 'WORKOUT_SCHEDULE' })
export class WorkoutSchedule {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'WORKOUTPLANID' })
  workoutPlanId: number

  @Column({ name: 'SCHEDULE_DATE' })
  scheduleDate: Date
}
