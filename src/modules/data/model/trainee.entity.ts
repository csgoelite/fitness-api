import { Workout } from './workout.entity'
import { Entity, Column, ManyToMany, JoinTable, PrimaryGeneratedColumn, OneToMany } from 'typeorm'
import { Trainer } from './trainer.entity'

@Entity({ name: 'TRAINEE' })
export class Trainee {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'NAME' })
  name: string

  @Column({ name: 'AGE' })
  age: number

  @Column({ name: 'EXTUSERID' })
  extUserId: string

  @Column({ name: 'WEIGHT' })
  weight: number

  @Column({ name: 'HEIGHT' })
  height: number

  @ManyToMany(type => Trainer, trainer => trainer.trainees)
  @JoinTable({
    name: 'TRAINER_TRAINEE',
  })
  trainers: Trainer[]

  @OneToMany(type => Workout, workout => workout.trainee)
  workouts: Workout[]
}
