import { AssignedExercise } from './assigned-exercise.entity'
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm'

@Entity({ name: 'SET_INFO' })
export class SetInfo {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'REPS' })
  reps: number

  @Column({ name: 'WEIGHT' })
  weight: number

  @ManyToOne(type => AssignedExercise, assignedExercise => assignedExercise.setInfos)
  @JoinColumn({ name: 'ASSIGNED_EXERCISEID' })
  assignedExercise: Partial<AssignedExercise>
}
