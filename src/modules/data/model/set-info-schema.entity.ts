import {AssignedExerciseSchema} from './assigned-exercise-schema.entity'
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm'

@Entity({ name: 'SET_INFO_SCHEMA' })
export class SetInfoSchema {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'REPS' })
  reps: number

  @Column({ name: 'WEIGHT' })
  weight: number

  @ManyToOne(type => AssignedExerciseSchema, assignedExerciseSchema => assignedExerciseSchema.setInfoSchemas)
  @JoinColumn({ name: 'ASSIGNED_EXERCISE_SCHEMAID' })
  assignedExerciseSchema: Partial<AssignedExerciseSchema>
}
