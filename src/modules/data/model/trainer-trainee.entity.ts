import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity({ name: 'TRAINER_TRAINEE' })
export class TrainerTrainee {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'TRAINERID' })
  trainerId: string

  @Column({ name: 'TRAINEEID' })
  traineeId: string

  @Column({ name: 'RELATION_STATUS'})
  relationStatus: string
}
