import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity({ name: 'MUSCLE_TYPE_BASE_EXERCISE' })
export class MuscleTypeBaseExercise {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'MUSCLE_TYPEID' })
  muscleTypeId: string

  @Column({ name: 'BASE_EXERCISEID' })
  baseExerciseId: string
}
