import { Trainer } from './trainer.entity'
import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, OneToMany, OneToOne } from 'typeorm'
import { AssignedExerciseSchema } from './assigned-exercise-schema.entity'

@Entity({ name: 'WORKOUT_SCHEMA' })
export class WorkoutSchema {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'CREATED' })
  created: Date

  @Column({ name: 'NAME' })
  name: string

  @OneToOne(type => Trainer)
  @JoinColumn({ name: 'TRAINERID' })
  trainer: Partial<Trainer>

  @OneToMany(type => AssignedExerciseSchema, assignedExerciseSchema => assignedExerciseSchema.workoutSchema)
  assignedExerciseSchemas: Partial<AssignedExerciseSchema[]>
}
