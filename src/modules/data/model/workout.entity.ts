import { Trainee } from './trainee.entity'
import { AssignedExercise } from './assigned-exercise.entity'
import { WorkoutPlan } from './workout-plan.entity'
import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, ManyToOne, OneToMany } from 'typeorm'

@Entity({ name: 'WORKOUT' })
export class Workout {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'FINISHED' })
  finished: Date

  @Column({ name: 'IS_EXTENDED' })
  isExtended: boolean

  @ManyToOne(type => WorkoutPlan, workoutPlan => workoutPlan.workouts)
  @JoinColumn({ name: 'WORKOUTPLANID' })
  workoutPlan: Partial<WorkoutPlan>

  @ManyToOne(type => Trainee, trainee => trainee.workouts)
  @JoinColumn({ name: 'TRAINEEID' })
  trainee: Partial<Trainee>

  @OneToMany(type => AssignedExercise, assignedExercise => assignedExercise.workout)
  assignedExercises: AssignedExercise[]
}
