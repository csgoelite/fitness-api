import { Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm'
import { BaseExercise } from './base-exercise.entity'
import { SetInfo } from './set-info.entity'
import { Workout } from './workout.entity'

@Entity({ name: 'ASSIGNED_EXERCISE' })
export class AssignedExercise {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @OneToOne(type => BaseExercise)
  @JoinColumn({ name: 'BASE_EXERCISEID' })
  baseExercise: Partial<BaseExercise>

  @ManyToOne(type => Workout, workout => workout.assignedExercises)
  @JoinColumn({ name: 'WORKOUTID' })
  workout: Partial<Workout>

  @OneToMany(type => SetInfo, setInfo => setInfo.assignedExercise)
  setInfos: SetInfo[]
}
