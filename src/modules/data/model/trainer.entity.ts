import {WorkoutSchema} from './workout-schema.entity'
import {BaseExercise} from './base-exercise.entity'
import { WorkoutPlan } from './workout-plan.entity'
import { Entity, Column, ManyToMany, PrimaryGeneratedColumn, OneToMany } from 'typeorm'
import { Trainee } from './trainee.entity'

@Entity({ name: 'TRAINER' })
export class Trainer {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'NAME' })
  name: string

  @Column({ name: 'AGE' })
  age: number

  @Column({ name: 'EXTUSERID' })
  extUserId: string

  @ManyToMany(type => Trainee, trainee => trainee.trainers)
  trainees: Trainee[]

  @OneToMany(type => WorkoutPlan, workoutPlan => workoutPlan.trainer)
  workoutPlans: WorkoutPlan[]

  @OneToMany(type => WorkoutSchema, workoutSchema => workoutSchema.trainer)
  workoutSchemas: WorkoutSchema[]

  @OneToMany(type => BaseExercise, baseExercise => baseExercise.trainer)
  baseExercises: BaseExercise[]
}
