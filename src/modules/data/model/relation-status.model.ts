export enum RelationStatus {
  ACCEPTED = 'ACCEPTED',
  PENDING = 'PENDING',
}
