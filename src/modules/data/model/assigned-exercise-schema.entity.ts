import { Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm'
import { BaseExercise } from './base-exercise.entity'
import { SetInfoSchema } from './set-info-schema.entity'
import { WorkoutSchema } from './workout-schema.entity'

@Entity({ name: 'ASSIGNED_EXERCISE_SCHEMA' })
export class AssignedExerciseSchema {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @OneToOne(type => BaseExercise)
  @JoinColumn({ name: 'BASE_EXERCISEID' })
  baseExercise: Partial<BaseExercise>

  @ManyToOne(type => WorkoutSchema, workoutSchema => workoutSchema.assignedExerciseSchemas)
  @JoinColumn({ name: 'WORKOUT_SCHEMAID' })
  workoutSchema: Partial<WorkoutSchema>

  @OneToMany(type => SetInfoSchema, setInfo => setInfo.assignedExerciseSchema)
  setInfoSchemas: SetInfoSchema[]
}
