import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { MuscleType } from './muscle-type.entity'
import { Trainer } from './trainer.entity'

@Entity({ name: 'BASE_EXERCISE' })
export class BaseExercise {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'NAME' })
  name: string

  @Column({ name: 'DESCRIPTION' })
  description: string

  @ManyToMany(type => MuscleType, muscleType => muscleType.baseExercises)
  @JoinTable({
    name: 'MUSCLE_TYPE_BASE_EXERCISE',
  })
  muscleTypes: MuscleType[]

  @ManyToOne(type => Trainer, trainer => trainer.baseExercises)
  @JoinColumn({ name: 'TRAINERID' })
  trainer: Partial<Trainer>
}
