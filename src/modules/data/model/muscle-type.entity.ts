import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm'
import { BaseExercise } from './base-exercise.entity'

@Entity({ name: 'MUSCLE_TYPE' })
export class MuscleType {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'NAME' })
  name: string

  @ManyToMany(type => BaseExercise, baseExercise => baseExercise.muscleTypes)
  baseExercises: BaseExercise[]
}
