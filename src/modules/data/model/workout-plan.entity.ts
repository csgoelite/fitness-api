import { Workout } from './workout.entity'
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm'
import { Trainer } from './trainer.entity'

@Entity({ name: 'WORKOUTPLAN' })
export class WorkoutPlan {

  @PrimaryGeneratedColumn({ name: 'ID' })
  id: string

  @Column({ name: 'NAME' })
  name: string

  @Column({ name: 'CREATED' })
  created: Date

  @ManyToOne(type => Trainer, trainer => trainer.workoutPlans)
  @JoinColumn({ name: 'TRAINERID' })
  trainer: Partial<Trainer>

  @OneToMany(type => Workout, workout => workout.workoutPlan)
  workouts: Partial<Workout[]>
}
