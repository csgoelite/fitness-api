import { TrainerTraineeRepository } from './repository/trainer-trainee.repository'
import { Global, Module } from '@nestjs/common'
import { AssignedExerciseSchemaRepository } from './repository/assigned-exercise-schema.repository'
import { AssignedExerciseRepository } from './repository/assigned-exercise.repository'
import { BaseExerciseRepository } from './repository/base-exercise.repository'
import { MuscleTypeBaseExerciseRepository } from './repository/muscle-type-base-exercise.repository'
import { SetInfoSchemaRepository } from './repository/set-info-schema.repository'
import { SetInfoRepository } from './repository/set-info.repository'
import { TraineeRepository } from './repository/trainee.repository'
import { TrainerRepository } from './repository/trainer.repository'
import { WorkoutPlanRepository } from './repository/workout-plan.repository'
import { WorkoutSchemaRepository } from './repository/workout-schema.repository'
import { WorkoutRepository } from './repository/workout.repository'

@Global()
@Module({
  providers: [
    AssignedExerciseSchemaRepository,
    AssignedExerciseRepository,
    BaseExerciseRepository,
    MuscleTypeBaseExerciseRepository,
    SetInfoSchemaRepository,
    SetInfoRepository,
    TraineeRepository,
    TrainerRepository,
    TrainerTraineeRepository,
    WorkoutPlanRepository,
    WorkoutRepository,
    WorkoutSchemaRepository,
  ],
  exports: [
    AssignedExerciseSchemaRepository,
    AssignedExerciseRepository,
    BaseExerciseRepository,
    MuscleTypeBaseExerciseRepository,
    SetInfoSchemaRepository,
    SetInfoRepository,
    TraineeRepository,
    TrainerRepository,
    TrainerTraineeRepository,
    WorkoutPlanRepository,
    WorkoutRepository,
    WorkoutSchemaRepository,
  ],
})
export class DataModule {}
