import { SetInfo } from './../model/set-info.entity'
import { Injectable } from '@nestjs/common'
import { getRepository } from 'typeorm'

@Injectable()
export class SetInfoRepository {

  async addSetInfo(setInfo: Partial<SetInfo>): Promise<SetInfo> {
    return await getRepository(SetInfo).save(setInfo)
  }
}
