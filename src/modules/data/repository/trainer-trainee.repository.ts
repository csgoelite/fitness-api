import { Injectable } from '@nestjs/common'
import { getRepository, UpdateResult, DeleteResult } from 'typeorm'
import { TrainerTrainee } from '../model/trainer-trainee.entity'
import { RelationStatus } from '../model/relation-status.model'

@Injectable()
export class TrainerTraineeRepository {

  async getTrainerStatusByTrainerIdAndTraineeId(trainerId: string, traineeId: string): Promise<TrainerTrainee> {
    return await getRepository(TrainerTrainee)
      .createQueryBuilder('trainerTrainee')
      .select([
        'trainerTrainee.id',
        'trainerTrainee.relationStatus',
      ])
      .where('trainerTrainee.trainerId = :trainerId', { trainerId })
      .andWhere('trainerTrainee.traineeId = :traineeId', { traineeId })
      .getOne()
  }

  async requestTrainer(traineeId: string, trainerId: string) {
    const trainerTrainee: Partial<TrainerTrainee> = { traineeId, trainerId, relationStatus: RelationStatus.PENDING }

    return await getRepository(TrainerTrainee).save(trainerTrainee)
  }

  async acceptTrainee(trainerId: string, traineeId: string) {
    let result: UpdateResult =  await getRepository(TrainerTrainee)
      .createQueryBuilder()
      .update(TrainerTrainee)
      .set({ relationStatus: 'ACCEPTED' })
      .where('trainerId = :trainerId', { trainerId })
      .andWhere('traineeId = :traineeId', { traineeId })
      .execute()

    if (result.raw.affectedRows === 0) {
      result = undefined
    }

    return result
  }

  async rejectTrainee(trainerId: string, traineeId: string) {
    let result: DeleteResult =  await getRepository(TrainerTrainee)
      .createQueryBuilder()
      .delete()
      .where('trainerId = :trainerId', { trainerId })
      .andWhere('traineeId = :traineeId', { traineeId })
      .execute()

    if (result.raw.affectedRows === 0) {
      result = undefined
    }

    return result
  }
}
