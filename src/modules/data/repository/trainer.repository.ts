import { MuscleType } from './../model/muscle-type.entity'
import { Injectable } from '@nestjs/common'
import { getRepository, UpdateResult } from 'typeorm'
import { Trainer } from '../model/trainer.entity'
import { Trainee } from '../model/trainee.entity'

@Injectable()
export class TrainerRepository {

  async getTrainers(): Promise<Trainer[]> {
    return await getRepository(Trainer)
      .createQueryBuilder('trainer')
      .select(this.getTrainerBaseQuery())
      .getMany()
  }

  async getTrainer(id: string): Promise<Trainer> {
    return await getRepository(Trainer)
      .createQueryBuilder('trainer')
      .select(this.getTrainerBaseQuery())
      .where('trainer.id = :id', { id })
      .getOne()
  }

  async getTrainerWithTrainees(id: string): Promise<Trainer> {
    return await getRepository(Trainer)
      .createQueryBuilder('trainer')
      .select(this.getTrainerBaseQuery())
      .where('trainer.id = :id', { id })
      .leftJoinAndSelect(
        'TRAINER_TRAINEE',
        'trainerTrainee',
        'trainerTrainee.TRAINERID = trainer.id',
      )
      .leftJoinAndMapMany(
        'trainer.trainees',
        Trainee,
        'trainee',
        'trainee.id = trainerTrainee.TRAINEEID',
      )
      .getOne()
  }

  async getTrainerWithWorkoutPlans(id: string): Promise<Trainer> {
    return await getRepository(Trainer)
      .createQueryBuilder('trainer')
      .select(this.getTrainerBaseQuery())
      .where('trainer.id = :id', { id })
      .leftJoinAndSelect(
        'trainer.workoutPlans',
        'workoutPlan',
      )
      .getOne()
  }

  async getTrainerWithBaseExercises(id: string): Promise<Trainer> {
    return await getRepository(Trainer)
      .createQueryBuilder('trainer')
      .select(this.getTrainerBaseQuery())
      .where('trainer.id = :id', { id })
      .leftJoinAndSelect(
        'trainer.baseExercises',
        'baseExercises',
      ).leftJoinAndSelect(
        'MUSCLE_TYPE_BASE_EXERCISE',
        'muscleTypeBaseExercise',
        'muscleTypeBaseExercise.BASE_EXERCISEID = baseExercises.id',
      ).leftJoinAndMapMany(
        'baseExercises.muscleTypes',
        MuscleType,
        'muscleType',
        'muscleType.id = muscleTypeBaseExercise.MUSCLE_TYPEID',
      )
      .getOne()
  }

  async getTrainerWithWorkoutSchemas(id: string): Promise<Trainer> {
    return await getRepository(Trainer)
      .createQueryBuilder('trainer')
      .select(this.getTrainerBaseQuery())
      .where('trainer.id = :id', { id })
      .leftJoinAndSelect(
        'trainer.workoutSchemas',
        'workoutSchemas',
      )
      .getOne()
  }

  async addTrainer(trainer: Partial<Trainer>) {
    return await getRepository(Trainer).save(trainer)
  }

  async updateTrainer(id: string, trainer: Partial<Trainer>) {
    let result: UpdateResult = await getRepository(Trainer).update(id, trainer)

    if (result.raw.affectedRows === 0) {
      result = undefined
    }

    return result
  }

  private getTrainerBaseQuery(): string[] {
    return ['trainer.id', 'trainer.name', 'trainer.age', 'trainer.id']
  }
}
