import { MuscleType } from './../model/muscle-type.entity'
import { Injectable } from '@nestjs/common'
import { getRepository } from 'typeorm'
import { BaseExercise } from '../model/base-exercise.entity'

@Injectable()
export class BaseExerciseRepository {

  async getBaseExercises(): Promise<BaseExercise[]> {
    return await getRepository(BaseExercise)
      .createQueryBuilder('baseExercise')
      .select([
        'baseExercise.id',
        'baseExercise.name',
        'baseExercise.description',
      ]).leftJoinAndSelect(
        'MUSCLE_TYPE_BASE_EXERCISE',
        'muscleTypeBaseExercise',
        'muscleTypeBaseExercise.BASE_EXERCISEID = baseExercise.id',
      ).leftJoinAndMapMany(
        'baseExercise.muscleTypes',
        MuscleType,
        'muscleType',
        'muscleType.id = muscleTypeBaseExercise.MUSCLE_TYPEID',
      )
      .getMany()
  }

  async addBaseExercise(baseExercise: Partial<BaseExercise>): Promise<BaseExercise> {
    return await getRepository(BaseExercise).save(baseExercise)
  }
}
