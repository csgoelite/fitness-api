import { getRepository } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { MuscleTypeBaseExercise } from './../model/muscle-type-base-exercise.entity'

@Injectable()
export class MuscleTypeBaseExerciseRepository {

  async addMuscleTypeBaseExerciseRelation(muscleTypeBaseExercise: Partial<MuscleTypeBaseExercise>) {
    return await getRepository(MuscleTypeBaseExercise).save(muscleTypeBaseExercise)
  }
}
