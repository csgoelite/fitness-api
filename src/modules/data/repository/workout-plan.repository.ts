import { MuscleType } from './../model/muscle-type.entity'
import { WorkoutPlan } from '../model/workout-plan.entity'
import { Injectable } from '@nestjs/common'
import { getRepository, DeleteResult } from 'typeorm'

@Injectable()
export class WorkoutPlanRepository {

  async getWorkoutPlans(): Promise<WorkoutPlan[]> {
    return await getRepository(WorkoutPlan)
    .createQueryBuilder('workoutplan')
    .select([
      'workoutplan.id',
      'workoutplan.name',
      'workoutplan.created',
    ])
    .leftJoinAndSelect(
      'workoutplan.trainer',
      'workoutPlanTrainers',
    ).leftJoinAndSelect(
      'workoutplan.workouts',
      'workoutPlanWorkouts',
    )
    .getMany()
  }

  async getWorkoutPlan(id: string): Promise<WorkoutPlan> {
    return await getRepository(WorkoutPlan)
    .createQueryBuilder('workoutplan')
    .select([
      'workoutplan.id',
      'workoutplan.name',
      'workoutplan.created',
    ])
    .where('workoutplan.id = :id', { id })
    .leftJoinAndSelect(
      'workoutplan.trainer',
      'workoutPlanTrainer',
    ).leftJoinAndSelect(
      'workoutplan.workouts',
      'workoutPlanWorkouts',
    ).leftJoinAndSelect(
      'workoutPlanWorkouts.trainee',
      'workoutPlanWorkoutTrainee',
    ).leftJoinAndSelect(
      'workoutPlanWorkouts.assignedExercises',
      'workoutPlanWorkoutAssignedExercises',
    ).leftJoinAndSelect(
      'workoutPlanWorkoutAssignedExercises.setInfos',
      'workoutPlanWorkoutAssignedExercisesSetInfos',
    ).leftJoinAndSelect(
      'workoutPlanWorkoutAssignedExercises.baseExercise',
      'baseExercise',
    ).leftJoinAndSelect(
      'MUSCLE_TYPE_BASE_EXERCISE',
      'muscleTypeBaseExercise',
      'muscleTypeBaseExercise.BASE_EXERCISEID = baseExercise.id',
    ).leftJoinAndMapMany(
      'baseExercise.muscleTypes',
      MuscleType,
      'muscleType',
      'muscleType.id = muscleTypeBaseExercise.MUSCLE_TYPEID',
    )

    .getOne()
  }

  async addWorkoutPlan(workoutPlan: Partial<WorkoutPlan>): Promise<WorkoutPlan> {
    return await getRepository(WorkoutPlan).save(workoutPlan)
  }

  async deleteWorkoutPlan(workoutPlanId: string): Promise<DeleteResult> {
    let result = await getRepository(WorkoutPlan).delete(workoutPlanId)

    if (result.raw.affectedRows === 0) {
      result = undefined
    }

    return result
  }
}
