
import { Injectable } from '@nestjs/common'
import { getRepository, DeleteResult } from 'typeorm'
import { AssignedExerciseSchema } from '../model/assigned-exercise-schema.entity'

@Injectable()
export class AssignedExerciseSchemaRepository {

  async addAssignedExerciseSchema(assignedExerciseSchema: Partial<AssignedExerciseSchema>): Promise<AssignedExerciseSchema> {
    return await getRepository(AssignedExerciseSchema).save(assignedExerciseSchema)
  }

  async getAssignedExerciseIdsByWorkoutSchemaId(workoutSchemaId: string): Promise<AssignedExerciseSchema[]> {
    return await getRepository(AssignedExerciseSchema)
      .createQueryBuilder('assignedExerciseSchema')
      .select(['assignedExerciseSchema.id'])
      .leftJoinAndSelect(
        'assignedExerciseSchema.workoutSchema',
        'workoutSchema',
      )
      .where('workoutSchema.id = :workoutSchemaId', { workoutSchemaId })
      .getMany()
  }

  async deleteAssignedExerciseSchema(id: string): Promise<DeleteResult> {
    return await getRepository(AssignedExerciseSchema).delete(id)
  }
}
