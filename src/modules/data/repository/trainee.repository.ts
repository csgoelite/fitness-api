import { Injectable } from '@nestjs/common'
import { getRepository } from 'typeorm'
import { Trainee } from '../model/trainee.entity'

@Injectable()
export class TraineeRepository {

  async getTrainees(): Promise<Trainee[]> {
    return await getRepository(Trainee)
      .createQueryBuilder('trainee')
      .select([
        'trainee.id',
        'trainee.name',
        'trainee.age',
        'trainee.id',
        'trainee.weight',
        'trainee.height',
      ])
      .getMany()
  }

  async getTrainee(id: string): Promise<Trainee> {
    return await getRepository(Trainee)
      .createQueryBuilder('trainee')
      .select([
        'trainee.id',
        'trainee.name',
        'trainee.age',
        'trainee.id',
        'trainee.weight',
        'trainee.height',
      ])
      .where('trainee.ID = :id', { id })
      .getOne()
  }

  async addTrainee(trainee: Partial<Trainee>) {
    return await getRepository(Trainee).save(trainee)
  }
}
