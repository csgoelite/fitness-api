import { SetInfoSchema } from './../model/set-info-schema.entity'
import { Injectable } from '@nestjs/common'
import { getRepository, DeleteResult, UpdateResult } from 'typeorm'

@Injectable()
export class SetInfoSchemaRepository {

  async getSetInfoSchemaIdsByAssignedExerciseId(assignedExerciseId: string): Promise<SetInfoSchema[]> {
    return await getRepository(SetInfoSchema)
      .createQueryBuilder('setInfoSchema')
      .select(['setInfoSchema.id'])
      .where('setInfoSchema.assignedExerciseSchema.id = :assignedExerciseId', { assignedExerciseId })
      .getMany()
  }

  async addSetInfoSchema(setInfoSchema: Partial<SetInfoSchema>): Promise<SetInfoSchema> {
    return await getRepository(SetInfoSchema).save(setInfoSchema)
  }

  async updateSetInfoSchema(id: string, setInfoSchema: Partial<SetInfoSchema>): Promise<UpdateResult> {
    return await getRepository(SetInfoSchema).update(id, setInfoSchema)
  }

  async deleteSetInfoSchema(id: string): Promise<DeleteResult> {
    return await getRepository(SetInfoSchema).delete(id)
  }
}
