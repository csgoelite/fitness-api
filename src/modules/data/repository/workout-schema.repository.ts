import { Injectable } from '@nestjs/common'
import { getRepository, UpdateResult, DeleteResult } from 'typeorm'
import { WorkoutSchema } from '../model/workout-schema.entity'
import { MuscleType } from '../model/muscle-type.entity'

@Injectable()
export class WorkoutSchemaRepository {

  async getWorkoutSchema(id: string): Promise<WorkoutSchema> {
    return await getRepository(WorkoutSchema)
    .createQueryBuilder('workoutSchema')
    .select([
      'workoutSchema.id',
      'workoutSchema.name',
      'workoutSchema.created',
    ])
    .where('workoutSchema.id = :id', { id })
    .leftJoinAndSelect(
      'workoutSchema.trainer',
      'workoutSchemaTrainer',
    ).leftJoinAndSelect(
      'workoutSchema.assignedExerciseSchemas',
      'workoutSchemaAssignedExerciseSchemas',
    ).leftJoinAndSelect(
      'workoutSchemaAssignedExerciseSchemas.baseExercise',
      'baseExercise',
    ).leftJoinAndSelect(
      'MUSCLE_TYPE_BASE_EXERCISE',
      'muscleTypeBaseExercise',
      'muscleTypeBaseExercise.BASE_EXERCISEID = baseExercise.id',
    ).leftJoinAndMapMany(
      'baseExercise.muscleTypes',
      MuscleType,
      'muscleType',
      'muscleType.id = muscleTypeBaseExercise.MUSCLE_TYPEID',
    ).leftJoinAndSelect(
      'workoutSchemaAssignedExerciseSchemas.setInfoSchemas',
      'setInfoSchemas',
    ).getOne()
  }

  async getWorkoutSchemas(): Promise<WorkoutSchema[]> {
    return await getRepository(WorkoutSchema)
    .createQueryBuilder('workoutSchema')
    .select([
      'workoutSchema.id',
      'workoutSchema.name',
      'workoutSchema.created',
    ]).leftJoinAndSelect(
      'workoutSchema.trainer',
      'workoutSchemaTrainer',
    ).leftJoinAndSelect(
      'workoutSchema.assignedExerciseSchemas',
      'workoutSchemaAssignedExerciseSchemas',
    ).leftJoinAndSelect(
      'workoutSchemaAssignedExerciseSchemas.baseExercise',
      'baseExercise',
    ).leftJoinAndSelect(
      'MUSCLE_TYPE_BASE_EXERCISE',
      'muscleTypeBaseExercise',
      'muscleTypeBaseExercise.BASE_EXERCISEID = baseExercise.id',
    ).leftJoinAndMapMany(
      'baseExercise.muscleTypes',
      MuscleType,
      'muscleType',
      'muscleType.id = muscleTypeBaseExercise.MUSCLE_TYPEID',
    ).leftJoinAndSelect(
      'workoutSchemaAssignedExerciseSchemas.setInfoSchemas',
      'setInfoSchemas',
    ).getMany()
  }

  async addWorkoutSchema(workoutSchema: Partial<WorkoutSchema>): Promise<WorkoutSchema> {
    return await getRepository(WorkoutSchema).save(workoutSchema)
  }

  async updateWorkoutSchema(id: string, workoutSchema: Partial<WorkoutSchema>): Promise<UpdateResult> {
    let result: UpdateResult = await getRepository(WorkoutSchema).update(id, workoutSchema)

    if (result.raw.affectedRows === 0) {
      result = undefined
    }

    return result
  }

  async deleteWorkoutSchema(id: string): Promise<DeleteResult> {
    let result = await getRepository(WorkoutSchema).delete(id)

    if (result.raw.affectedRows === 0) {
      result = undefined
    }

    return result
  }
}
