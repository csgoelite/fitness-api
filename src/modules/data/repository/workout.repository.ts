import { Workout } from './../model/workout.entity'
import { Injectable } from '@nestjs/common'
import { getRepository } from 'typeorm'

@Injectable()
export class WorkoutRepository {

  async getWorkouts(): Promise<Workout[]> {
    return await getRepository(Workout)
    .createQueryBuilder('workout')
    .select([
      'workout.id',
      'workout.finished',
      'workout.isExtended',
    ])
    .leftJoinAndSelect(
      'workout.workoutPlan',
      'workoutWorkoutPlan',
    ).leftJoinAndSelect(
      'workout.trainee',
      'workoutTrainee',
    ).leftJoinAndSelect(
      'workout.assignedExercises',
      'workoutAssignedExercises',
    )
    .getMany()
  }

  async addWorkout(workout: Partial<Workout>): Promise<Workout> {
    return await getRepository(Workout).save(workout)
  }

}
