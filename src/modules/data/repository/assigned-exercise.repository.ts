import { AssignedExercise } from './../model/assigned-exercise.entity'
import { Injectable } from '@nestjs/common'
import { getRepository } from 'typeorm'

@Injectable()
export class AssignedExerciseRepository {

  async addAssignedExercise(assignedExercise: Partial<AssignedExercise>): Promise<AssignedExercise> {
    return await getRepository(AssignedExercise).save(assignedExercise)
  }
}
