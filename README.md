## Description

If you want to use the remote mysql server, you need to create an ssh tunnel.

```bash
$ ssh root@95.179.147.80 -L 3307:localhost:3306 -N
```

When it asks for password enter (dM9Bu*JYqkt59#w

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

